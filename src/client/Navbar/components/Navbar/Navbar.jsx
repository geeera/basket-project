import React from "react";
import "./Navbar.css";

import img from "../../../../assests/svg/001-shopping-cart.svg";
import logo from "../../../../assests/svg/001-science.svg";
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {sidebarAction} from "../../../../store/actions/sidebarAction";

const Navbar = () => {
    const price = useSelector(({basket: {price}}) => price)
    const dispatch = useDispatch()
    const openBar = () => dispatch(sidebarAction(false))
    const closeBar = () => dispatch(sidebarAction(true))


    return(
        <header className="header">
            <NavLink to="/" className='logo-link'>
                <h1 className="nav-logo"><img src={logo} alt="logo" className="img"/>My Portfolio</h1>
            </NavLink>
            <nav className="navbar">
                <NavLink to='/' exact className="link" activeClassName="active-link" onClick={closeBar}>Home</NavLink>
                <NavLink to='/products' exact className="link" activeClassName="active-link" onClick={closeBar}>Products</NavLink>
            </nav>
            <div className="basket-block">
                <img src={img} alt="Basket" onClick={openBar} className="basket"/>
                <span className="basket-total-price">{price} $</span>
            </div>
        </header>
    )
}

export default Navbar
