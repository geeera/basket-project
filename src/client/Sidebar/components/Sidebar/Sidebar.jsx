import React, {useEffect} from "react";
import ReactDOM from "react-dom";
import img from "../../../../assests/svg/002-remove.svg";

import "./Sidebar.css";
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {sidebarAction} from "../../../../store/actions/sidebarAction";
import BasketItem from "../BasketItem";
import {clearBasketAction} from "../../../../store/actions/clearBasketAction";
import {totalPriceAction} from "../../../../store/actions/getTotalPrice";


const Sidebar = () => {
    const {isClose} = useSelector(({bar}) => bar, shallowEqual)
    const {list, price} = useSelector(({basket}) => basket, shallowEqual)
    const dispatch = useDispatch()
    const closeBar = () => {
        dispatch(sidebarAction(true))
    }

    const clearBasket = () => {
        dispatch(clearBasketAction())
    }

    useEffect(() => {
        dispatch(totalPriceAction())
    }, [list, dispatch])


    const isHide = isClose ? 'hide' : ''

    const productList = list.map((item) => <BasketItem key={item.id} {...item} />)
    const content = (
        <div className={"sidebar " + isHide}>
            <header className="sidebar-header">
                <h2 className="sidebar-header__title">Your Basket</h2>
                <img className="basket-hide" alt='hide' src={img} onClick={closeBar} />
            </header>
            <main className="sidebar-main">
                {productList}
            </main>
            <footer className="sidebar-footer">
                {!!productList.length && <button className="sidebar-footer__btn" onClick={clearBasket}>Clear Basket</button>}
                {!!productList.length && <button className="sidebar-footer__btn">Pay</button>}
                <h4 className="sidebar-footer__price">Total Price: {price}$</h4>
            </footer>
        </div>)
    return ReactDOM.createPortal(content, document.getElementById('side-hook'))
}

export default Sidebar
