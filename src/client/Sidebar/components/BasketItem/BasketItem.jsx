import React from "react";
import "./BasketItem.css";

import minus from "../../../../assests/svg/001-minus-symbol.svg";
import plus from "../../../../assests/svg/002-plus.svg";
import {useDispatch} from "react-redux";
import {decreaseCountAction} from "../../../../store/actions/decreaceCountAction";
import {addToBasketAction} from "../../../../store/actions/addToBasketAction";

const BasketItem = (props) => {
    const {image, title, price, count, id} = props
    const dispatch = useDispatch()
    const increaseCount = () => dispatch(addToBasketAction(props))
    const decreaseCount = () => dispatch(decreaseCountAction(id))
    return(
        <div className="basket-item" id={id}>
            <img src={image} alt={title} className="basket-image"/>
            <div className="basket-product">
                <h4 className="basket-title">{title}</h4>
                {/*<p className="basket-description">{description}</p>*/}
                <div className="basket-info">
                    <span className="basket-price">Price: {price}$</span>
                    <div className="basket-count">
                        <img src={minus} onClick={decreaseCount} alt="minus" className="count-minus"/>
                            {count}
                        <img src={plus} onClick={increaseCount} alt="plus" className="count-plus"/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default BasketItem
