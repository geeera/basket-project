import React from "react";
import "./ProductItem.css";
import {useDispatch} from "react-redux";
import {addToBasketAction} from "../../../../store/actions/addToBasketAction";

const ProductItem = (props) => {
    const {image, title, description, price} = props
    const dispatch = useDispatch()
    const addToBasket = (e) => {
        e.preventDefault()
        dispatch(addToBasketAction({
            ...props,
            "start-price": price
        }))
    }

    return(
        <div className="product">
            <div className="product-image__block">
                <img src={image} alt="" className="product-image"/>
                <h4 className="product-title">{title}</h4>
                <p className="product-description">{description}</p>
            </div>
            <footer className="product-footer">
                <button className="product-btn" onClick={addToBasket}>To Basket</button>
                <h4 className="product-price">{price}$</h4>
            </footer>
        </div>
    )
}

export default ProductItem
