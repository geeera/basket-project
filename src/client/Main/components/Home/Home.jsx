import React from "react";
import "./Home.css";

import img from "../../../../assests/jpeg/IMG_0788.jpeg";

const Home = () => {
    return(
        <div className="home-page">
            <div className="profile">
                <div className="profile-image">
                    <img src={img} alt="#" className="profile-image__img"/>
                </div>
                <div className="profile-user">
                    <h2 className="user-full-name">Gerasimenko Kirill</h2>
                    <p className="user-description">
                        Hello everybody, my name is Kirill and this is my a little work.
                        it is analog a shopping cart. This is application was build with ReactJS,
                        and use the global storage Redux.
                    </p>
                    <p className="user-description">
                        So, if you will be interesting and, maybe, you wont to ask with me,
                        My tel.: 050-142-61-45.
                    </p>
                </div>
            </div>
        </div>
    )
}

export default Home
