import React from "react";
import "./Products.css";
import ProductItem from "../ProductItem";
import {shallowEqual, useSelector} from "react-redux";

const Products = () => {
    const list = useSelector(({list}) => list, shallowEqual)
    const products = list.map((item, idx) => {
        return <ProductItem key={"product-" + idx} data-col={item.col} {...item}/>
    })

    return(
        <div className="products-page">
            <div className="product-list">
                <div className="product-col">{products.filter(({props: {"data-col": col}}) => col === 1)}</div>
                <div className="product-col">{products.filter(({props: {"data-col": col}}) => col === 2)}</div>
                <div className="product-col">{products.filter(({props: {"data-col": col}}) => col === 3)}</div>
            </div>
        </div>
    )
}

export default Products
