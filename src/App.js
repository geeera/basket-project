import React from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import './App.css';
import Navbar from "./client/Navbar/components/Navbar";
import Main from "./client/Main/components/Main";
import {Provider} from "react-redux";
import {store} from "./store/store";

function App() {
  return (
      <Provider store={store}>
        <Router>
            <Navbar />
            <Main />
        </Router>
      </Provider>
  );
}

export default App;
