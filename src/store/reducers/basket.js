import {ADD_TO_BASKET, CLEAR_BASKET, DECREASE_COUNT, GET_TOTAL_PRICE} from "../constants";

const basket = {
    list: [],
    price: 0
}

export const basketReducer = (state = basket, action) => {
    const {list} = {...state}
    switch (action.type) {
        case ADD_TO_BASKET:
            const {list: newList} = {...state}
            const elem = newList.find(({index}) => index === action.payload.index)
            if (elem) {
                elem.count++
                elem.price = elem.count * elem["start-price"]
                localStorage.setItem('basket-list', JSON.stringify(newList))
                return {
                    ...state,
                    list: [...newList]
                }
            }
            const newConcat = [...newList, action.payload]
            localStorage.setItem('basket-list', JSON.stringify(newConcat))
            return {
                ...state,
                list: newConcat
            }
        case DECREASE_COUNT:
            const minusCountElem = list.find(({id}) => id === action.payload)
            minusCountElem.count--
            minusCountElem.price = minusCountElem.count * minusCountElem["start-price"]
            if (!minusCountElem.count) {
                const newList = list.filter(({id}) => id !== action.payload)
                localStorage.setItem('basket-list', JSON.stringify(newList))
                return {
                    ...state,
                    list: [...newList]
                }
            }
            localStorage.setItem('basket-list', JSON.stringify(list))
            return {
                ...state,
                list: [...list]
            }
        case CLEAR_BASKET :
            localStorage.removeItem('basket-list')
            return {
                ...state,
                list: []
            }
        case GET_TOTAL_PRICE:
            let price = list.reduce((arr, item) => (item["start-price"] * item.count) + arr, 0)
            return {
                ...state,
                price
            }

        default:
            const defaultList = JSON.parse(localStorage.getItem('basket-list'))
            return {
                ...state,
                list: defaultList || []
            }
    }
}
