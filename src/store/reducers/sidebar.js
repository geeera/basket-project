import {TOGGLE_BAR} from "../constants";

const initState = {
    isClose: true
}

export const sidebarReducer = (state = initState, action) => {
    switch (action.type) {
        case TOGGLE_BAR:
            // console.log(action)
            return {
                ...state,
                isClose: action.payload
            }
        default:
            return state
    }
}
