import {combineReducers} from "redux";

import {productsReducer} from "./products";
import {sidebarReducer} from "./sidebar";
import {basketReducer} from "./basket";

export const rootReducer = combineReducers({
    list: productsReducer,
    bar: sidebarReducer,
    basket: basketReducer
})
