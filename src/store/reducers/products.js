import grass from "../../assests/jpeg/grass.jpeg";
import way from "../../assests/jpeg/way.jpeg";
import art from "../../assests/jpeg/photo_art.jpeg";
import {v4} from 'uuid';

const initState = [
    {
        title: 'Photo of Grass',
        image: grass,
        description: 'Some a beautiful picture',
        price: 3,
        col: 1,
        id: v4(),
        index: "card-1",
        count: 1
    },
    {
        title: 'Sunny Way',
        image: way,
        description: 'Some a beautiful picture',
        price: 2,
        col: 2,
        id: v4(),
        index: "card-2",
        count: 1
    },
    {
        title: 'Great Art',
        image: art,
        description: 'Some a beautiful picture',
        price: 1,
        col: 3,
        id: v4(),
        index: "card-3",
        count: 1
    },
    {
        title: 'Photo of Grass',
        image: grass,
        description: 'Some a beautiful picture',
        price: 3,
        col: 3,
        id: v4(),
        index: "card-1",
        count: 1
    },
    {
        title: 'Sunny Way',
        image: way,
        description: 'Some a beautiful picture',
        price: 2,
        col: 1,
        id: v4(),
        index: "card-2",
        count: 1
    },
    {
        title: 'Great Art',
        image: art,
        description: 'Some a beautiful picture',
        price: 1,
        col: 2,
        id: v4(),
        index: "card-3",
        count: 1
    },
    {
        title: 'Photo of Grass',
        image: grass,
        description: 'Some a beautiful picture',
        price: 3,
        col: 2,
        id: v4(),
        index: "card-1",
        count: 1
    },
    {
        title: 'Sunny Way',
        image: way,
        description: 'Some a beautiful picture',
        price: 2,
        col: 3,
        id: v4(),
        index: "card-2",
        count: 1
    },
    {
        title: 'Great Art',
        image: art,
        description: 'Some a beautiful picture',
        price: 1,
        col: 1,
        id: v4(),
        index: "card-3",
        count: 1
    }
]

export const productsReducer = (state = initState, action) => {
    switch (action.type) {
        default:
            return state
    }
}
