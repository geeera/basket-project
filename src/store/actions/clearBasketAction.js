import {CLEAR_BASKET} from "../constants";

export const clearBasketAction = () => {
    return {
        type: CLEAR_BASKET
    }
}
