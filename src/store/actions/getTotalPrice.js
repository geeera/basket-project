import {GET_TOTAL_PRICE} from "../constants";

export const totalPriceAction = () => {
    return {
        type: GET_TOTAL_PRICE
    }
}
