import {ADD_TO_BASKET} from "../constants";

export const addToBasketAction = (payload) => {
    return {
        type: ADD_TO_BASKET,
        payload
    }
}
